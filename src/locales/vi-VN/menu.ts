export default {
  'menu.home': 'Trang chủ',
  // dashboard
  'menu.dashboard': 'Dashboard',
  // modules
  'menu.reports': 'Báo cáo',
  'menu.reports.onsite': 'Onsite',
  'menu.reports.ot': 'OT',
  'menu.reports.debit': 'Tạm ứng',
  // groups
  'menu.groups': 'Nhóm',
  // members
  'menu.members': 'Nhân sự',
  //
  'menu.salary': 'Bảng lương',
  // administrators
  'menu.projects': 'Dự án',
  // settings
  'menu.settings': 'Cài đặt',
  'menu.settings.general': 'Thông tin chung',
  'menu.settings.realState': 'Thông tin bất động sản',
  'menu.settings.advertisement': 'Quảng cáo',
  'menu.settings.news': 'Tin nổi bật',
  'menu.settings.coin': 'RecCoin',
  'menu.settings.account': 'Tài khoản',
}
