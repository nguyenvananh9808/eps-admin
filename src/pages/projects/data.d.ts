export type TypeProject = {
  _id?: any,
  name: string,
  state: string,
  start: any,
  createdAt?: string
  updatedAt?: string,
  end: any,
}

export type TypeListProject = {
  status: number,
  data?: TypeProject[],
  pageSize?: number,
  pageNo?: number,
  total?: number,
  message?: string
}

