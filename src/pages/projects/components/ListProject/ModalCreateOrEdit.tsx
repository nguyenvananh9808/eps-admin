import type { FC } from 'react'
import { useEffect } from 'react'
import { Button, DatePicker, Divider, Form, Input, Modal, Select, Space } from 'antd'
import type { Dispatch } from 'umi'
import { connect, useIntl } from 'umi'
import { CloseOutlined } from '@ant-design/icons'
import moment from 'moment'

type Props = {
  dispatch: Dispatch
  modalData: {
    modalType: 'create' | 'edit'
    record?: any
  }
  isVisibleModal: boolean
  setIsVisibleModal: (data: boolean) => void
}

const ModalCreateOrEdit: FC<Props> = ({
  dispatch,
  modalData,
  isVisibleModal,
  setIsVisibleModal,
}) => {
  const { formatMessage } = useIntl()
  const [form] = Form.useForm()

  useEffect(() => {
    if (!isVisibleModal) {
      form.resetFields()
    } else if (modalData.modalType === 'edit') {
      form.setFieldsValue({
        ...modalData.record,
        start: modalData.record.start ? moment(modalData.record.start, 'DD/MM/YYYY') : '',
        end: modalData.record.end ? moment(modalData.record.end, 'DD/MM/YYYY'): '',
      })
    }
  }, [form, isVisibleModal, modalData.modalType, modalData.record])

  const handleFinish = (values: any) => {
    if (modalData.modalType === 'create') {
      dispatch({
        type: 'projects/createProject',
        payload: {
          data: {
            ...values,
            start: moment(values.start).format('YYYY-MM-DD'),
            end: moment(values.end).format('YYYY-MM-DD'),
          },
        },
      })
    } else {
      dispatch({
        type: 'projects/editProject',
        payload: {
          id: modalData.record.key,
          data: {
            ...values,
            start: moment(values.start).format('YYYY-MM-DD'),
            end: moment(values.end).format('YYYY-MM-DD'),
          },
        },
      })
    }
    setIsVisibleModal(false)
  }

  return (
    <Modal
      title={formatMessage({
        id: modalData.modalType === 'create' ? 'button.create' : 'button.edit',
      })}
      visible={isVisibleModal}
      footer={null}
      closeIcon={<CloseOutlined onClick={() => setIsVisibleModal(false)} />}
      centered
    >
      <Form form={form} layout="vertical" onFinish={handleFinish}>
        <Form.Item
          name="name"
          label={formatMessage({ id: 'common.name' })}
          rules={[
            {
              required: true,
              message: formatMessage({ id: 'form.formItem.required.message' }),
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="state"
          rules={[
            {
              required: true,
              message: formatMessage({ id: 'form.formItem.required.message' }),
            },
          ]}
          label={formatMessage({ id: 'common.status' })}
        >
          <Select placeholder={formatMessage({ id : 'common.select' })}>
            <Select.Option value="PENDING">
              {formatMessage({ id: 'project.status.pending' })}
            </Select.Option>
            <Select.Option value="DONE">
              {formatMessage({ id: 'project.status.done' })}
            </Select.Option>
            <Select.Option value="REJECT">
              {formatMessage({ id: 'project.status.reject' })}
            </Select.Option>
            <Select.Option value="DOING">
              {formatMessage({ id: 'project.status.doing' })}
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="start"
          label={formatMessage({ id: 'common.startAt' })}
          rules={[
            {
              required: true,
              message: formatMessage({ id: 'form.formItem.required.message' }),
            },
          ]}
        >
          <DatePicker format="DD/MM/YYYY" />
        </Form.Item>
        <Form.Item
          name="end"
          label={formatMessage({ id: 'common.finishAt' })}
          rules={[
            {
              required: modalData.modalType !== 'edit',
              message: formatMessage({ id: 'form.formItem.required.message' }),
            },
          ]}
        >
          <DatePicker format="DD/MM/YYYY" />
        </Form.Item>
        <Divider />
        <Form.Item className="mb--0">
          <Space className="w--full justify-content--flexEnd">
            <Button onClick={() => setIsVisibleModal(false)}>
              {formatMessage({ id: 'button.cancel' })}
            </Button>
            <Button htmlType="submit" type="primary">
              {formatMessage({
                id: modalData.modalType === 'create' ? 'button.create' : 'button.update',
              })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default connect()(ModalCreateOrEdit)
