export default {
  'project.title': 'Danh sách dự án',
  'project.status.doing': 'Đang làm',
  'project.status.done': 'Đã hoàn thành',
  'project.status.pending': 'Chờ',
  'project.status.reject': 'Hủy',
  'project.status.default': 'Không xác định',
}
