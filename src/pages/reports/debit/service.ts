import request from '@/utils/request'
import { API_URL } from '@/utils/utils'

type ParamType = {
  id?: number
  query?: string
  data?: any
}

export const getAllDebit = async (payload: ParamType) => {
  return request(`${API_URL}/debit${payload.query}`)
}

export const approvedOrRejectDebit = async (payload: ParamType) => {
  return request(`${API_URL}/debit/${payload.query}`, {
    method: 'POST',
    data: payload.data,
  })
}
export const searchDebit = async (payload: ParamType) => {
  return request(`${API_URL}/debit/${payload.query}`, {
    method: 'POST',
    data: payload.data,
  })
}

