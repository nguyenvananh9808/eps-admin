import { message } from 'antd'
import type { Effect, Reducer } from 'umi'
import { formatMessage } from 'umi'
import type { ListDebitType } from './data'
import {
  getAllDebit,
  approvedOrRejectDebit,
  searchDebit,
} from './service'

type Model = {
  namespace: 'debit'
  state: ListDebitType
  reducers: {
    saveListDebit: Reducer<ListDebitType>
  }
  effects: {
    getAllDebit: Effect
    approvedOrRejectDebit: Effect
    searchDebit: Effect
  }
}

export default <Model>{
  namespace: 'debit',
  state: {
    status: 200,
  },
  reducers: {
    saveListDebit(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: {
    *getAllDebit({ payload }, { call, put }) {
      try {
        const response = yield call(getAllDebit, payload)
        yield put({
          type: 'saveListDebit',
          payload: response,
        })
      } catch (error) {
        message.error(formatMessage({ id: 'error' }))
      }
    },
    *approvedOrRejectDebit({ payload }, { call }) {
      try {
        yield call(approvedOrRejectDebit, payload)
      } catch (error) {
        message.error(formatMessage({ id: 'error' }))
      }
    },
    *searchDebit({ payload }, { call, put }) {
      try {
        const response = yield call(searchDebit, payload)
        yield put({
          type: 'saveListDebit',
          payload: response,
        })
      } catch (error) {
        message.error(formatMessage({ id: 'error' }))
      }
    },
  },
}
