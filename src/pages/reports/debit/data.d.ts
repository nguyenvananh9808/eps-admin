export type OneDataDebit = {
  _id?: number
  employee?: { _id: string | '', name: string | '' }
  date?: string
  status?: string,
  amount?: number,
  content?: string,
}

export type ListDebitType = {
  status: number
  data?: OneDataDebit[]
  total?: number
  message?: string
}
