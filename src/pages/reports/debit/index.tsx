import type { FC } from 'react'
import { useState } from 'react'
import { GridContent } from '@ant-design/pro-layout'
import { ListDebit, SearchDebit } from './components'

type Props = {}

const News: FC<Props> = () => {
  const [condition, setCondition] = useState<any>()

  return (
    <GridContent>
      <div className="layout--search">
        <SearchDebit setCondition={setCondition} />
      </div>
      <div className="layout--main">
        <div className="layout--main__content">
          <ListDebit condition={condition} />
        </div>
      </div>
    </GridContent>
  )
}

export default News
