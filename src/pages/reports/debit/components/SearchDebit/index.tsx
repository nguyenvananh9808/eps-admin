import type { FC } from 'react'
import { Button, Col, DatePicker, Form, Input, Row, Select } from 'antd'
import { connect, useIntl } from 'umi'
import _ from 'lodash'
import { isMoment } from 'moment'

type Props = {
  setCondition: any
}
const { Option } = Select
const SearchDebit: FC<Props> = ({ setCondition }) => {
  const { formatMessage } = useIntl()
  const [form] = Form.useForm()

  const onSearch = (values: any) => {
    const data = {
      name: _.get(values, 'name', ''),
      status: values.status,
      from:
        !_.isEmpty(values.state) && isMoment(values.state[0])
          ? values.state[0].format('YYYY-MM-DD')
          : '',
      to:
        !_.isEmpty(values.state) && isMoment(values.state[1])
          ? values.state[1].format('YYYY-MM-DD')
          : '',
    }
    console.log('data values: ', data)
    setCondition(data)
  }

  return (
    <Form
      form={form}
      name="advanced_search"
      className="ant-advanced-search-form"
      onFinish={onSearch}
    >
      <Row gutter={20}>
        <Col span="4">
          <Form.Item name={`name`}>
            <Input placeholder={formatMessage({ id: 'common.name' })} />
          </Form.Item>
        </Col>
        <Col span="6">
          <Form.Item name={`state`}>
            <DatePicker.RangePicker
              format="DD/MM/YYYY"
              className="w--full"
              inputReadOnly
              placeholder={[
                formatMessage({ id: 'common.from' }),
                formatMessage({ id: 'common.to' }),
              ]}
            />
          </Form.Item>
        </Col>
        <Col span="2">
          <Form.Item name={`status`}>
            <Select style={{ width: 120 }}>
              <Option value="APPROVAL">Đã duyệt</Option>
              <Option value="WAIT">Chờ duyệt</Option>
              <Option value="REJECT">Từ chối</Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={6} style={{ textAlign: 'right' }}>
          <Button type="primary" htmlType="submit">
            {formatMessage({ id: 'button.search' })}
          </Button>
          <Button
            style={{ margin: '0 8px' }}
            onClick={() => {
              form.resetFields()
              setCondition(undefined)
            }}
          >
            {formatMessage({ id: 'button.reset' })}
          </Button>
        </Col>
      </Row>
    </Form>
  )
}

export default connect()(SearchDebit)
