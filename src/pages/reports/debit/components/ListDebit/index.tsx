import type { FC } from 'react'
import { useEffect, useState } from 'react'
import { Row, Col, Table, Button, Divider, Badge, Breadcrumb } from 'antd'
import type { Dispatch } from 'umi'
import { connect, FormattedMessage, useIntl } from 'umi'
import _ from 'lodash'
import moment from 'moment'
import type { ListDebitType, OneDataDebit } from '../../data'
import { modalApproved } from '@/utils/utils'
import { ModalReject } from '@/components'

type Props = {
  dispatch: Dispatch
  dataTable: ListDebitType
  approvedOrRejectDebit?: boolean
  condition: any
}

const ListDebit: FC<Props> = ({ dispatch, dataTable, condition, approvedOrRejectDebit }) => {
  const { formatMessage } = useIntl()
  const [selectedRowKeys, setSelectedRowKeys] = useState([])
  const [isVisibleModal, setIsVisibleModal] = useState(false)
  const [recordId, setRecordId] = useState<any>()
  const [pageNo, setPageNo] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    if (condition) {
      dispatch({
        type: 'debit/searchDebit',
        payload: {
          query: `?pageNo=${pageNo}&pageSize=${pageSize}`,
          data: condition,
        },
      })
    } else {
      dispatch({
        type: 'debit/getAllDebit',
        payload: {
          query: `?pageNo=${pageNo}&pageSize=${pageSize}`,
        },
      })
    }
    setLoading(false)
  }, [dispatch, pageNo, pageSize, condition])

  useEffect(() => {
    if (approvedOrRejectDebit) {
      setLoading(true)
    }
    if (approvedOrRejectDebit === false) {
      setSelectedRowKeys([])
      if (condition) {
        dispatch({
          type: 'debit/searchDebit',
          payload: {
            query: `?pageNo=${pageNo}&pageSize=${pageSize}`,
            data: condition,
          },
        })
      } else {
        dispatch({
          type: 'debit/getAllDebit',
          payload: {
            query: `?pageNo=${pageNo}&pageSize=${pageSize}`,
          },
        })
      }
      setRecordId(null)
      setLoading(false)
    }
  }, [approvedOrRejectDebit, dispatch, pageNo, pageSize, condition])

  const onChangeSelectRow = (values: any) => {
    setSelectedRowKeys(values)
  }

  const onChangePagination = (value: number) => {
    setPageNo(value)
    setSelectedRowKeys([])
  }

  const onApprove = (id: string) => {
    const onOk = () => {
      dispatch({
        type: 'debit/approvedOrRejectDebit',
        payload: {
          query: id,
          data: {
            action: "APPROVAL",
          },
        },
      })
    }
    modalApproved(onOk)
  }
  const onReject = (id: string) => {
    setRecordId(id)
    setIsVisibleModal(true)
  }

  const request = (values: any) => {
    dispatch({
      type: 'debit/approvedOrRejectDebit',
      payload: {
        query: recordId,
        data: {
          action: "REJECT",
          reason: values,
        },
      },
    })
  }

  const displayApprove = (status: string) => {
    switch (status) {
      case "REJECT":
        return {
          text: 'Từ chối',
          status: 'error',
        }
      case "APPROVAL":
        return {
          text: 'Duyệt',
          status: 'success',
        }
      default:
        return {
          text: 'Chờ duyệt',
          status: 'warning',
        }
    }
  }

  const dataSource =
  dataTable?.data?.map((item: OneDataDebit) => ({
    key: item._id,
    name: _.get(item, 'employee', { name: '' })?.name,
    status: item.status,
    dateDebit: item.date && moment(item.date).format('DD/MM/YYYY'),
    amount: item.amount,
    content: item.content,
  })) || []
  const columns: any = [
    {
      title: formatMessage({ id: 'common.name' }),
      dataIndex: 'name',
      key: 'name',
      render: (value: string) =>
        <Button className="pl--0" type="link" htmlType="button" >
          {value}
        </Button>,
    },
    {
      title: 'Ngày tạm ứng',
      dataIndex: 'dateDebit',
      key: 'dateDebit',
    },
    {
      title: 'Tiền tạm ứng',
      dataIndex: 'amount',
      key: 'amount',
    },
    {
      title: formatMessage({ id: 'common.status' }),
      dataIndex: 'status',
      key: 'status',
      render: (_value: string) => {
        const status: any = displayApprove(_value)
        return <Badge {...status} />
      },
    },
    {
      title: formatMessage({ id: 'common.action' }),
      key: 'action',
      render: (value: string, record: any) => (
        <div className="display--flex justify-content--between">
          <Button className="pl--0" type="link" htmlType="button" disabled={record.status === "APPROVAL"} onClick={() => onApprove(record.key)}>
            Duyệt
          </Button>
          <Button className="pl--0" type="link" htmlType="button" disabled={record.status === "REJECT"} onClick={() => onReject(record.key)} danger>
            Từ chối
          </Button>
        </div>
      ),
      width: 100,
    },
  ]

  return (
    <>
      <Breadcrumb separator=">" className="layout--main__title">
        <Breadcrumb.Item><FormattedMessage id="report" /></Breadcrumb.Item>
        <Breadcrumb.Item><FormattedMessage id="report.debit" /></Breadcrumb.Item>
      </Breadcrumb>
      <Divider />
      <Row gutter={24}>
        <Col span={24}>
          <div className="layout--table">
            <Table
              rowSelection={{
                selectedRowKeys,
                onChange: onChangeSelectRow,
                fixed: true,
              }}
              scroll={{
                x: true,
              }}
              pagination={{
                size:"small",
                position: ['bottomRight'],
                showSizeChanger: true,
                total: dataTable?.total,
                onChange: (value: number) => onChangePagination(value),
                showTotal: (total: number, range: any) =>`${range[0]} - ${range[1]} of ${total} items`,
                current: pageNo,
                pageSize,
                onShowSizeChange: (current: number, size: number) => setPageSize(size),

              }}
              dataSource={dataSource}
              columns={columns}
              loading={loading}
            />
          </div>
        </Col>
      </Row>
      <ModalReject
        onOk={request}
        isVisibleModal={isVisibleModal}
        setIsVisibleModal={setIsVisibleModal}
      ></ModalReject>
    </>
  )
}

export default connect(
  ({
    debit,
    loading,
  }: {
    debit: ListDebitType
    loading: {
      effects: Record<string, boolean>
    }
  }) => ({
    dataTable: debit,
    approvedOrRejectDebit: loading.effects['debit/approvedOrRejectDebit'],
  }),
)(ListDebit)
