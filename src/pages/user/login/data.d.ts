export type TypeUserAndLogin = {
  status: number
  data?: {
    accessToken: string,
    name: string
    role?: string
    avatar?: {
      url: string
    }
  }
  message?: string
}
